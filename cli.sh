#!/bin/bash 

# stop on exit.
set e 

export COLLECTION_UUID=3271570-9db21acd-9b54-47b5-8489-384496f31676
export API_KEY=23055e4ff1884557962fd50f08dd8ecc
export ENVIRONMENT_UUID=3271570-88c22463-a7b4-4000-927c-74eba15bab73

echo "Downloading collection and environments"
curl --request GET --url "https://api.getpostman.com/collections/$COLLECTION_UUID" --header "X-Api-Key: $API_KEY" > "collection.json"
curl --request GET --url "https://api.getpostman.com/environments/$ENVIRONMENT_UUID" --header "X-Api-Key: $API_KEY" > "environment.json"


./node_modules/newman/bin/newman.js run "collection.json" \
  --insecure \
  --environment "environment.json" \
  --iteration-data "iteration-data/missing_data.csv" \
  --folder "Missing Data" \
  --delay-request 3000 \
  --bail failure \
  --reporters cli

 ./node_modules/newman/bin/newman.js run "collection.json" \
  --insecure \
  --environment "environment.json" \
  --iteration-data "iteration-data/exclusions/Hitachi.csv" \
  --folder "Funder Exclusion Scenarios" \
  --delay-request 3000 \
  --bail failure \
  --reporters cli

 ./node_modules/newman/bin/newman.js run "collection.json" \
  --insecure \
  --environment "environment.json" \
  --iteration-data "iteration-data/exclusions/Alphera.csv" \
  --folder "Funder Exclusion Scenarios" \
  --delay-request 3000 \
  --bail failure \
  --reporters cli


 ./node_modules/newman/bin/newman.js run "collection.json" \
  --insecure \
  --environment "environment.json" \
  --iteration-data "iteration-data/exclusions/Santander.csv" \
  --folder "Funder Exclusion Scenarios" \
  --delay-request 3000 \
  --bail failure \
  --reporters cli