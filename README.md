
# Newman Runner

This little script just automates the running of the Zoot Test scripts. 

### Operating instructions

`npm install` 

Install all the things. 


### Setup
We need to download and run some collections locally in the Jenkins build. Ideally, we'd pull from the API. But, this works well for now.

To get started edit `download.js` and fill in the correct UUID's for the collection / environment you require. 
You'll need to grab the UUID of that collection or environment. You can do that in Postman by using the collection called Postman API. There should be calls in there that list our environments and collection details and their associated meta-data.

The default UUIDs point to the UAT environment.

### Collection Configs
Each collection also has config, we ideally should refactor all this to make the calls more dynamic. The collection config documents what input data and what folder to run as part of the collection run. Collection config can be easily edited. These configs are also used in the `download.js` script to pull data into a local folder. 
```
"/path/to/iteration-data.csv" : {
  "folder" : "Name of folder to run",
  "delay" : "delay between requests",
  "name" : "display name",
  "uuid" : "uuid of the collection in Postman API"
}
```

### Iteration Data
We pass in CSV file for each iteration. These are stored in the `./iteration-data/` folder. Each file must have a matching config in `collections-config`
 

### Adding more collections 
Once you're happy with the collection and tests in the Postman UI. You can alter the `runner.js` to include the new collection and iteration data.


### Example

* So, the best place to start is the Postman UI - create a folder inside the ESB Automation collection. You'll probably be best duplicating one of the existing folders.
* You should read through this: [Working with Data Files]
(https://www.getpostman.com/docs/postman/collection_runs/working_with_data_files)
* Prepare a CSV file with the fields you want to send in each iteration. 

Modify the collections, collection as appropriate.

```
  "my-new-suite/data.csv" : {
    "name": "New Data Suite",
    "folder": "Folder name inside ESB Automation Collection",
    "uuid": collectionUuid
  },
```

### Running the Tests

Please run `node download.js` first. This should pull in all files needed to kick off a run.

Then 
`node runner.js /relative/path/to/iteration-data.csv `

This path argument must exist in the `collection-config.js` as this argument will be used to grab the collection and the folder.

With hindsight we are literally wrapping the Newman Runner, therefore we could probably do away with most of the code, but for now this exists to run all tests.

### Code
* It's ES6 / NodeJS - That's the only version of Newman available. No .NET here. 
* It's using the [Airbnb Style Guide](https://github.com/airbnb/javascript) - which is pretty strict. 2 spaces, trailing commas etc. ESLint will let you know all your failings.
